package com.example.p6l2

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.support.v4.app.TaskStackBuilder
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        showDetail.setOnClickListener {
            val myDetailIntent = Intent(this, DetailActivity::class.java)
            startActivity(myDetailIntent)

        }
        doAsync {
            Thread.sleep(5000L)
            uiThread {
                showNotivy()
            }
        }
    }

    private fun showNotivy() {
        val notfyDetailIntent = Intent(this, DetailActivity::class.java)
        val myPendingIntent = TaskStackBuilder.create(this)
            .addParentStack(DetailActivity::class.java)
            .addNextIntent(notfyDetailIntent)
            .getPendingIntent(110, PendingIntent.FLAG_UPDATE_CURRENT)

        val myNotfyManager = this.getSystemService(android.content.Context.NOTIFICATION_SERVICE) as NotificationManager
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val channelId = "asd"
            val channelName = "test"
            val importance = NotificationManager.IMPORTANCE_LOW

            val mChannel = NotificationChannel(
                channelId, channelName, importance
            )
            myNotfyManager.createNotificationChannel(mChannel)
        }
        val myBuilder = NotificationCompat.Builder(this, "asd")
            .setContentTitle("Show Detail Contact")
            .setContentText("Klik Me")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentIntent(myPendingIntent)
            .setAutoCancel(true)
        myNotfyManager.notify(1101, myBuilder.build())
    }

}
